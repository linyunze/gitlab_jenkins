package com.example.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.domain.ResponObject;
import com.example.domain.User;
import com.example.service.UserService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponObject add(@RequestBody User user) {
        val isSuccess = userService.save(user);
        if (isSuccess){
            return new ResponObject("添加成功",true);
        }else {
            return new ResponObject("添加失败",false);
        }
    }

    @GetMapping
    public ResponObject find(int page,int size) {
        val pageUser = userService.page(new Page<>(page, size), null);
        val userList = Optional.ofNullable(pageUser)
                .map(Page::getRecords)
                .orElseGet(ArrayList::new);
        return new ResponObject(userList);
    }

    @PutMapping
    public ResponObject update(User user) {
        val isSuccess = userService.update(user, null);
        if (isSuccess){
            return new ResponObject("修改成功",true);
        }else {
            return new ResponObject("修改失败",false);
        }
    }

    @DeleteMapping
    public ResponObject delete(String userId) {
        val isSuccess = userService.removeById(userId);
        if (isSuccess){
            return new ResponObject("删除成功",true);
        }else {
            return new ResponObject("删除失败",false);
        }
    }

}
