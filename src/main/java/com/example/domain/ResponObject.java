package com.example.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ResponObject<T> {
    /**
     * 数据
     */
    private T data;

    /**
     * 返回信息
     */
    private String message;

    /**
     * 是否成功：true，成功；false，失败
     */
    private Boolean isSuccess;

    public ResponObject(T data, String message,Boolean isSuccess) {
        this.data = data;
        this.message = message;
        this.isSuccess = isSuccess;
    }

    public ResponObject(T data, String message) {
        this.data = data;
        this.message = message;
        this.isSuccess = true;
    }

    public ResponObject(String message,Boolean isSuccess) {
        this.data = null;
        this.message = message;
        this.isSuccess = isSuccess;
    }

    public ResponObject(T data) {
        this.data = data;
        this.message="成功";
        this.isSuccess = true;
    }
}
