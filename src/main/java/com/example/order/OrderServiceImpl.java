package com.example.order;

import lombok.val;

import java.util.concurrent.*;

public class OrderServiceImpl {

    /**
     * 异步提交订单
     */
    private String ayscSubmit() {
        val pool = Executors.newCachedThreadPool();
        pool.execute(this::submitOrder);
        return "订单已提交成功但未处理完成";
    }

    /**
     * 模拟提交订单业务
     */
    private void callBack() {
        System.out.println("============通知成功业务============");
    }

    /**
     * 模拟提交订单业务
     */
    private void submitOrder() {
        System.out.println("============订单提交业务============");
        //完成后调用通知业务
        callBack();
    }
}
